#!/usr/bin/env python
# coding: utf-8

# # Data Science
# #### By: Overpower Gore
# [license-badge]: https://img.shields.io/badge/License-CC-orange
# [license]: https://creativecommons.org/licenses/by-nc-sa/3.0/deed.en
# 

# In[3]:


#importing different libraries annd packages to be able to manipulate the datasets

import matplotlib.pyplot as plt
import pandas as pd
import pylab as pl
import numpy as np
get_ipython().run_line_magic('matplotlib', 'inline')


# # Part I: Simpler Linear Regression. Knowing data

# Data source [0].

# <h1>Table of contents</h1>
# 
# <div class="alert  alert-block alert-info" style="margin-top: 20px">
#     <ol>
#         <li><a href="#unData">Data</a></li>
#          <ol>
#              <li><a href="#reData">Reading</a></li>
#              <li><a href="#exData">Exploration</a></li>
#          </ol>
#         <li><a href="#daExploration">Data Exploration</a></li>
#         <li><a href="#simRegression">Simple Regression Model</a></li>
#     </ol>
# </div>
# <br>
# <hr>

# 
# <h2 id="unData">Data</h2>
# 
# ### `FuelConsumption.csv`:
# 
# This dataset contains a model-specific fuel consumption ratings and estimated carbon dioxide 
# emissions for new light-duty vehicles for retail sale in Canada.
# 
# Some **features** are
# 
# - **MODELYEAR** e.g. 2014
# - **MAKE** e.g. Acura
# - **MODEL** e.g. ILX
# - **VEHICLE CLASS** e.g. SUV
# - **ENGINE SIZE** e.g. 4.7
# - **CYLINDERS** e.g 6
# - **TRANSMISSION** e.g. A6
# - **FUEL CONSUMPTION in CITY(L/100 km)** e.g. 9.9
# - **FUEL CONSUMPTION in HWY (L/100 km)** e.g. 8.9
# - **FUEL CONSUMPTION COMB (L/100 km)** e.g. 9.2
# - **CO2 EMISSIONS (g/km)** e.g. 182   --> low --> 0

# In[5]:


#how to open a csv file to understand the contents of the file 
df = pd.read_csv("FuelConsumption.csv")


# <h3>Dataframe</h3>
# 
# <div class="alert  alert-block alert-info" style="margin-top: 20px">
#     A DataFrame represents a rectangular table of data and contains an ordered collection 
#     of columns, each of which can be a different value type (numeric, string, boolean, etc.). 
#     The DataFrame has both a row and column index; it can be thought of as a dict 
#     of Series all sharing the same index. Under the hood, the data is stored as one or 
#     more two-dimensional blocks rather than a list, dict, or some other collection of 
#     one-dimensional arrays.
# </div>
# <br>
# <hr>

# In[6]:


#Checking the datatypes of the different columns in the dataframe
df.dtypes


# In[7]:


#checking the size in terms of shape, how many rows and columns
df.shape


# In[8]:


#displaying the rows and the number of columns in the dataframe through the use of print statements
print("Number of rows =", df.shape[0], "\nNumber of features (columns) =",df.shape[1])


# In[9]:


#list of columns in the dataframe using the columns feature
df.columns


# In[10]:


#types of the datafram using the type() function 
type(df)


# In[11]:


#using the head() function to view the first few rows of the dataframe
df.head()


# In[12]:


#using the head() function to view the first 7 rows of the dataframe
df.head(7)


# In[13]:


#understanding the data throigh descriptive statistics using the describe() function
df.describe()


# ## Querying
# 
# Note, pandas considers a table (dataframe) as a pasting of many "series" together, horizontally.

# In[14]:


#using type to see the datatypes of the dataframe and the modelyear
type(df.MODELYEAR), type(df)


# In[15]:


#filtering the based on the enginesize
df.ENGINESIZE <= 2


# In[16]:


#checking the sum of the dataframe's enginesize less than 2
SumEng = np.sum(df.ENGINESIZE <= 2)
SumEng


# In[17]:


#checking the average using sum/total number of engines of the dataframe's enginesize less than 2
SumEngTotal = np.sum(df.ENGINESIZE <= 2)/df.shape[0]
SumEngTotal


# In[18]:


#checking the average using the mean function number of engines of the dataframe's enginesize less than 2
MeanTotal = np.mean(df.ENGINESIZE <= 2.0)
MeanTotal


# In[19]:


#checking the average using the mean function with a different syntax number of engines of the dataframe's enginesize less than 2
EngMean = (df.ENGINESIZE <= 2).mean()
EngMean


# In[20]:


#checking the average using the average function number of engines of the dataframe's enginesize less than 2
AverageEng = np.average(df.ENGINESIZE <= 2.0)
AverageEng


# In[21]:


#the above 4 modules all produce the same answer because in the backend all do the same thing.
#I am confident of this because I know the mean, the average and sum/lenght all produce the same thing but all use a different syntax
# Owing to this reason, all the answers to the above 4 modules are the same. 


# In[23]:


#finding the median of the dataframe using the median function of all the whole engine size dataset
MedianEng = np.median(df.ENGINESIZE)
MedianEng


# In[24]:


#checking the average using the average function number of engines of the dataframe's enginesize
AverageEng = np.average(df.ENGINESIZE)
AverageEng


# In[25]:


#checking the average using the mean function number of engines of the dataframe's enginesize
MeanEng = np.average(df.ENGINESIZE)
MeanEng


# In[ ]:


#checking the sum of the dataframe's enginesize less than 2
SumEng = np.sum(df.ENGINESIZE <= 2)
SumEng


# In[26]:


#checking the variance using the mean function number of engines of the dataframe's enginesize
VarianceEng = np.var(df.ENGINESIZE)
VarianceEng


# In[27]:


#checking the standard deviation using the mean function number of engines of the dataframe's enginesize
StdEng = np.std(df.ENGINESIZE)
StdEng


# In[28]:


#checking the max using the mean function number of engines of the dataframe's enginesize
MaxEng = np.max(df.ENGINESIZE)
MaxEng


# ##  Exercises

# 1. Why previous outputs are same?
# 1. Use at least four more features and calculate: average, mean, median, sum, and implement at least three more statistics functions. Check the ```numpy``` and ```pandas``` documentation.
# 1. Submmit your report in Moodle. Template https://www.overleaf.com/read/xqcnnnrsspcp

# ## Versions

# In[1]:


from platform import python_version
print("python version: ", python_version())
get_ipython().system('pip3 freeze | grep qiskit')


# # References

# [0] data https://tinyurl.com/2m3vr2xp
# 
# [1] numpy https://numpy.org/
# 
# [2] scipy https://docs.scipy.org/
# 
# [3] matplotlib https://matplotlib.org/
# 
# [4] matplotlib.cm https://matplotlib.org/stable/api/cm_api.html
# 
# [5] matplotlib.pyplot https://matplotlib.org/stable/api/pyplot_summary.html
# 
# [6] pandas https://pandas.pydata.org/docs/
# 
# [7] seaborn https://seaborn.pydata.org/
# 

# In[ ]:




